﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaxSMTI.Models;
using MaxSMTI.Instances;
using MaxSMTI.Algorithms;


namespace MaxSMTI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void generateInstanceButton_Click(object sender, EventArgs e)
        {           

            var ssmti = new Smi(Properties.Resources.sample2);

            TimeSpan timespan = TimeSpan.Zero;
            Match sm = null; ;
                        
            switch (comboBox1.SelectedIndex)
            {
                case 0:                    
                    var algorithm = new Halldorson(ssmti);
                    var timer = Stopwatch.StartNew();
                    sm = algorithm.SetStableMarriage();
                    timer.Stop();
                    timespan = timer.Elapsed;
                    break;
                case 1:                    
                    var algorithm2 = new IrvingManloveApproximation(ssmti);
                    timer = Stopwatch.StartNew();
                    sm = algorithm2.SetStableMarriage();
                    timer.Stop();
                    timespan = timer.Elapsed;
                    break;
                case 2:                    
                    var algorithm3 = new IwamaMiyazakiYamauchi(ssmti);
                    timer = Stopwatch.StartNew();
                    sm = algorithm3.SetStableMarriage();
                    timer.Stop();
                    timespan = timer.Elapsed;
                    break;
                default:
                    break;
            }

            var instanceInfo = new Info(ssmti);

            maxSmtiLabel.Text = sm != null ? sm.GetSize().ToString() : "-";
            runningTimeLabel.Text = (timespan != TimeSpan.Zero) ? timespan.ToString() : "-";
            cardinalLabel.Text = instanceInfo.GetCardinality() != 0 ? instanceInfo.GetCardinality().ToString() : "-";
            prefSizeLabel.Text = instanceInfo.GetPreferenceListLengthAverage() != 0 ? instanceInfo.GetPreferenceListLengthAverage().ToString() : "-";
            tieLabel.Text = instanceInfo.GetTieCount() != 0 ? instanceInfo.GetTieCount().ToString() : "-";
            pictureBox1.Visible = true;
            
            


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            maxSmtiLabel.Text = "-";
            runningTimeLabel.Text = "-";
            cardinalLabel.Text = "-";
            prefSizeLabel.Text = "-";
            tieLabel.Text = "-";
            pictureBox1.Visible = false;
        }
    }
}
