﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    class BoundedList<TE> where TE : class
    {
        private BoundedListNode<TE> _startNode;
        private BoundedListNode<TE> _lastUsedNode;
        protected Dictionary<TE, BoundedListNode<TE>> Pointer;

        
        public BoundedList(int capacity)
        {
            Pointer = new Dictionary<TE, BoundedListNode<TE>>(capacity);
            _startNode = new BoundedListNode<TE>(null);
            _lastUsedNode = _startNode;
        }

        public void AddElement(TE element)
        {
            if (Contains(element))
            {
                throw new ArgumentException(
                        "A lista már tartalmazza a(z) " + element + " elemet.");
            }
            var node = new BoundedListNode<TE>(element);
            AddNode(node);
        }

        public void AddElementAfter(TE oldElement, TE newElement)
        {
            if (!Contains(oldElement))
            {
                throw new ArgumentException("A(z) " + oldElement
                        + " elem nem található.");
            }
            if (Contains(newElement))
            {
                return;
            }
            var node = GetNode(oldElement);
            var newNode = new BoundedListNode<TE>(newElement);
            newNode.SetPrevious(node);
            newNode.SetNext(node.GetNext());
            if (node.GetNext() != null)
            {
                node.GetNext().SetPrevious(newNode);
            }
            node.SetNext(newNode);                        

            Pointer.Add(newElement, newNode);
        }

        public bool RemoveElement(TE element)
        {
            var node = Pointer[element];
            return RemoveNode(node);
        }

        protected void AddNode(BoundedListNode<TE> node)
        {
            _lastUsedNode.SetNext(node);
            node.SetPrevious(_lastUsedNode);
            _lastUsedNode = node;
            Pointer.Add(node.GetContent(), node);
        }

        protected bool RemoveNode(BoundedListNode<TE> node)
        {
            if (node == null)
            {
                return false;
            }
            var previous = node.GetPrevious();
            var next = node.GetNext();
            if (previous != null)
                previous.SetNext(next);
            if (next != null)
                next.SetPrevious(previous);
            Pointer.Remove(node.GetContent());
            return true;
        }

        protected BoundedListNode<TE> GetFirstNode()
        {
            return _startNode.GetNext();
        }

        protected BoundedListNode<TE> GetNode(TE element)
        {
            return Pointer[element];
        }

        public bool Contains(TE element)
        {
            if (Pointer.Count() == 0) return false;
            if (!Pointer.ContainsKey(element)) return false;
            return Pointer[element] != null;
        }

        public bool IsEmpty()
        {
            return _startNode.GetNext() == null;
        }

        protected bool ChangePointer(TE oldElement, TE newElement)
        {
            var node = Pointer[oldElement];
            if (node == null)
            {
                return false;
            }
            if (newElement == null)
            {
                throw new NullReferenceException("Az új mutató értéke null.");
            }
            Pointer.Remove(newElement);
            Pointer.Add(newElement, node);
            return true;
        }
    }
}
