﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    internal class Male : Person
    {
        public Male(int id, int cardinality) : base(id, Sex.Male, cardinality)
        {
        }

        public Female GetNextFemale()
        {
            return GetNextFemale(true);
        }

        public Female GetNextFemale(bool moveForwardOnPreferenceList)
        {
            return (Female)GetNextPerson(moveForwardOnPreferenceList);
        }

        public bool HasAlreadyProposedToEveryFemale()
        {
            return HasAlreadyProposedToAllPersons();
        }

    }
}
