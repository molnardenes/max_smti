﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    internal abstract class Person : IComparable<Person>
    {
        private Sex _sex;
        private int _id;
        private PreferenceList _preferenceList;
        private Person _lastProposedPerson;

        protected Person(int id, Sex sex, int cardinality)
        {
            _id = id;
            _sex = sex;
            _preferenceList = new PreferenceList(this, cardinality);
        }

        public int GetId()
        {
            return _id;
        }

        public Sex GetSex()
        {
            return _sex;
        }

        public bool IsMale()
        {
            return _sex == Sex.Male;
        }

        public bool IsFemale()
        {
            return _sex == Sex.Female;
        }

        public PreferenceList GetPreferenceList()
        {
            return _preferenceList;
        }

        public bool RemoveFromPreferenceList(Person person)
        {
            return _preferenceList.Remove(person);
        }

        public void RemoveMutually(Person person)
        {
            RemoveFromPreferenceList(person);
            person.RemoveFromPreferenceList(this);
        }

        public bool RemoveWorseFromPreferenceList(Person person)
        {
            var personsForRemoving = new HashSet<Person>();
            var currentTie = _preferenceList.GetTie(person);
            var tie = _preferenceList.GetNextTie(currentTie);

            while (tie != null)
            {
                personsForRemoving.UnionWith(tie.GetPersons());
                tie = _preferenceList.GetNextTie(tie);
            }
            if (!personsForRemoving.Any())
            {
                return false;
            }

            foreach (var actualPerson in personsForRemoving)
            {
                RemoveMutually(actualPerson);
            }

            return true;
        }

        public int GetChoice(Person person)
        {
            if (_preferenceList.Contains(person))
            {
                return _preferenceList.GetTie(person).GetChoice();
            }
            return -1;
        }

        public bool LikesMore(Person firstPerson, Person secondPerson)
        {
            return LikesMore(firstPerson, 0, secondPerson, 0);
        }

        public bool LikesMore(Person firstPerson, double additionPriority1,
                Person secondPerson, double additionPriority2)
        {
            var firstPersonChoiceNumber = GetChoice(firstPerson);
            var secondPersonChoiceNumber = GetChoice(secondPerson);

            if (firstPersonChoiceNumber == -1)
            {
                return false;
            }
            if (secondPersonChoiceNumber == -1)
            {
                return true;
            }
            if (firstPersonChoiceNumber == secondPersonChoiceNumber)
            {
                return additionPriority1 > additionPriority2;
            }
            return firstPersonChoiceNumber < secondPersonChoiceNumber;
        }

        private Person GetNextPerson(Person person)
        {
            Tie currentTie;
            if (person == null)
            {
                currentTie = _preferenceList.GetFirstTie();
                return currentTie != null ? currentTie.GetPerson() : null;
            }
            else
            {
                currentTie = _preferenceList.GetTie(person);
            }
            if (currentTie == null)
            {
                return null;
            }
            if (currentTie.Size() > 1)
            {
                throw new InvalidOperationException("A személy egy döntetlen része.");
            }
            var nextTie = _preferenceList.GetNextTie(currentTie);

            if (nextTie == null)
            {
                return null;
            }
            if (nextTie.Size() > 1)
            {
                throw new InvalidOperationException("A(z) " + person
                        + " személy következő jelöltje döntetlen része: " + nextTie);
            }
            return nextTie.GetPerson();
        }

        public bool IsLastOnPreferenceList(Person person)
        {
            return GetNextPerson(person) == null;
        }

        public bool PromotePersonAhead(Person person)
        {
            var tie = _preferenceList.GetTie(person);
            if (tie == null)
            {
                return false;
            }
            var promotedPerson = new List<Person>(1);
            promotedPerson.Add(person);
            _preferenceList.BreakTie(tie, promotedPerson);
            return true;
        }

        public bool IsPersonInTie(Person person)
        {
            var tie = _preferenceList.GetTie(person);
            if (tie == null)
            {
                throw new InvalidOperationException("A döntetlen értéke null.");
            }
            return tie.Size() > 1;
        }

        public bool IsAcceptable(Person person)
        {
            return _preferenceList.Contains(person);
        }


        public void MoveForwardOnPreferenceList()
        {
            _lastProposedPerson = GetNextPerson(_lastProposedPerson);
        }


        public Person GetLastProposedWoman()
        {
            return _lastProposedPerson;
        }

        public void RestartProposing()
        {
            _lastProposedPerson = null;
        }

        public Person GetNextPerson()
        {
            return GetNextPerson(true);
        }

        public Person GetNextPerson(bool moveForwardOnPreferenceList)
        {
            var nextPerson = GetNextPerson(_lastProposedPerson);
            if (moveForwardOnPreferenceList)
            {
                MoveForwardOnPreferenceList();
            }
            return nextPerson;
        }

        protected Person GetFirstPerson()
        {
            var firstTie = _preferenceList.GetFirstTie();
            return firstTie != null ? firstTie.GetPerson() : null;
        }


        public bool HasAlreadyProposedToAllPersons()
        {
            return GetNextPerson(_lastProposedPerson) == null;
        }


        public override string ToString()
        {
            var people = new StringBuilder();

            foreach (var tie in _preferenceList)
            {
                if (tie.GetPersons().Count() > 1)
                    people.Append("(");

                foreach (var person in tie.GetPersons())
                {
                    people.Append(person.GetId() + " ");
                }

                if (tie.GetPersons().Count() > 1)
                    people.Append(") ");
            }

            return (_sex == Sex.Male ? "Férfi" : "Nő") + "(" + _id + "): " + people;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 179;

                hash = hash * 233 + _id.GetHashCode();
                hash = hash * 233 + _sex.GetHashCode();

                return hash;
            }
        }

        public override bool Equals(Object o)
        {
            if (!(o is Person)) return false;

            var person = (Person)o;
            return _id == person.GetId() && _sex == person.GetSex();
        }
        public int CompareTo(Person person)
        {
            return _id - person.GetId();
        }
    }
}
