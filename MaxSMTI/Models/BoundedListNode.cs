﻿using System;
namespace MaxSMTI.Models
{
    class BoundedListNode<TF> where TF : class
    {
        private BoundedListNode<TF> _previous;
        private BoundedListNode<TF> _next;
        private TF _content;

        public BoundedListNode(TF content)
        {
            this._content = content;
        }

        public BoundedListNode<TF> GetPrevious()
        {
            return _previous;
        }

        public void SetPrevious(BoundedListNode<TF> previous)
        {
            this._previous = previous;
        }

        public BoundedListNode<TF> GetNext()
        {
            return _next;
        }

        public void SetNext(BoundedListNode<TF> next)
        {
            this._next = next;
        }

        public TF GetContent()
        {
            return _content;
        }

        public void SetContent(TF content)
        {
            this._content = content;
        }

        public bool HasNext()
        {
            return _next != null;
        }

        
        public override string ToString()
        {
            return _content.ToString();
        }
    }
}