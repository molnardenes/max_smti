﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    internal class Match
    {
        private Dictionary<Male, Female> _maleSet = new Dictionary<Male, Female>();
        private Dictionary<Female, Male> _femaleSet = new Dictionary<Female, Male>();


        public Match() : this(new HashSet<MatchingPair>())
        {
        }

        public Match(Match match) : this(match.GetMatchingCouples())
        {
        }

        public Match(HashSet<MatchingPair> matchingPairs)
        {
            AddMatchingCouples(matchingPairs);
        }

        public bool IsInMarriage(Person firstPerson, Person secondPerson)
        {
            if (firstPerson == null || secondPerson == null)
            {
                return false;
            }
            return secondPerson.Equals(GetPartner(firstPerson));
        }

        public bool AddMatchingCouple(Male male, Female female)
        {
            if (male == null || female == null || IsInMarriage(male, female))
            {
                return false;
            }
            BreakMarriage(male);
            BreakMarriage(female);
            _maleSet.Add(male, female);
            _femaleSet.Add(female, male);
            return true;
        }

        public bool AddMatchingCouple(Person firstPerson, Person secondPerson)
        {
            Male male = null;
            Female female = null;
            if (firstPerson is Male)
            {
                male = (Male)firstPerson;
            }
            if (secondPerson is Male)
            {
                male = (Male)secondPerson;
            }
            if (firstPerson is Female)
            {
                female = (Female)firstPerson;
            }
            if (secondPerson is Female)
            {
                female = (Female)secondPerson;
            }
            
            return AddMatchingCouple(male, female);
        }


        public void BreakMarriage(Person person)
        {
            if (person is Male)
            {
                var male = (Male)person;
                var female = (Female)GetPartner(person);
                if (_femaleSet != null && female != null) _femaleSet.Remove(female);
                if (_maleSet != null && male != null) _maleSet.Remove(male);
            }
            if (person is Female)
            {
                var male = (Male)GetPartner(person);
                var female = (Female)person;
                if (_femaleSet != null && female != null) _femaleSet.Remove(female);
                if (_maleSet != null && male != null) _maleSet.Remove(male);
            }

        }

        public bool IsFree(Person person)
        {
            if (person is Male && _maleSet.ContainsKey((Male)person))
                return _maleSet[(Male)person] == null;
            else if (person is Female && _femaleSet.ContainsKey((Female)person))
                return _femaleSet[(Female)person] == null;
            else
            {
                return false;
            }

        }

        public bool Contains(MatchingPair pair)
        {
            return IsInMarriage(pair.GetMale(), pair.GetFemale());
        }

        public Person GetPartner(Person person)
        {
            var male = person as Male;
            if (male != null && _maleSet != null)
            {
                return !_maleSet.ContainsKey(male) ? null : _maleSet[male];
            }
            var female = person as Female;
            if (female != null && _femaleSet != null)
            {
                return !_femaleSet.ContainsKey(female) ? null : _femaleSet[female];
            }
            return null;
        }

        public void AddMatchingCouples(HashSet<MatchingPair> matchingPairs)
        {
            if (matchingPairs == null)
            {
                return;
            }
            foreach (var matchingPair in matchingPairs)
            {
                AddMatchingCouple(matchingPair);
            }
        }

        public bool AddMatchingCouple(MatchingPair matchingPair)
        {
            if (matchingPair == null)
            {
                throw new NullReferenceException("Null érték nem adható hozzá.");
            }
            return AddMatchingCouple(matchingPair.GetMale(), matchingPair.GetFemale());
        }

        public void RemoveMatchingCouple(HashSet<MatchingPair> matchingPairs)
        {
            if (matchingPairs == null)
            {
                return;
            }
            foreach (var matchingPair in matchingPairs)
            {
                RemoveMatchingCouple(matchingPair);
            }
        }

        public bool RemoveMatchingCouple(MatchingPair matchingPair)
        {
            if (matchingPair == null || !IsInMarriage(matchingPair.GetMale(), matchingPair.GetFemale()))
            {
                return false;
            }
            BreakMarriage(matchingPair.GetMale());
            return true;
        }

        public HashSet<MatchingPair> GetMatchingCouples()
        {
            var matchingPairs = new HashSet<MatchingPair>();
            if (_maleSet == null) return matchingPairs;
            foreach (var male in _maleSet.Keys)
            {
                matchingPairs.Add(new MatchingPair(male, GetPartner(male)));
            }

            return matchingPairs;
        }

        public List<Male> GetMales()
        {
            return _maleSet == null ? null : _maleSet.Keys.ToList();
        }

        public List<Female> GetFemales()
        {
            return _femaleSet == null ? null : _femaleSet.Keys.ToList();
        }

        public int GetSize()
        {
            return _maleSet == null ? 0 : _maleSet.Count();
        }


        public override String ToString()
        {
            var sb = new StringBuilder("[ ");
            foreach (var matchingCouple in GetMatchingCouples())
            {
                sb.Append("(Férfi " + matchingCouple.GetMale().GetId() + ", Nő " + matchingCouple.GetFemale().GetId() +
                          ") ");
            }
            sb.Append("]");

            return sb.ToString();
        }
    }
}
