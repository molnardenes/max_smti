﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    class Female : Person
    {
        public Female(int id, int cardinality) : base(id, Sex.Female, cardinality)
        {
        }

        public Male GetFirstMale()
        {
            return (Male) GetFirstPerson();
        }
    }
}
