﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaxSMTI.Models
{    
    class BipartiteGraph
    {
        private HashSet<Person> _males;
        private HashSet<Person> _females;
        private Dictionary<Person, HashSet<Person>> _graph;

        public BipartiteGraph(HashSet<Person> males, HashSet<Person> females) : this(males, females, new Dictionary<Person, HashSet<Person>>())
        { }

        public BipartiteGraph(HashSet<Person> males, HashSet<Person> females, Dictionary<Person, HashSet<Person>> edges)
        {
            _males = males;
            _females = females;
            _graph = new Dictionary<Person, HashSet<Person>>();
            foreach (var firstPerson in edges.Keys)
            {
                foreach (var secondPerson in edges[firstPerson])
                {
                    AddEdge(firstPerson, secondPerson);
                }
            }
        }

        public bool AddEdge(Person firstPerson, Person secondPerson)
        {
            if ((!_males.Contains(firstPerson) || !_females.Contains(secondPerson))
                    && (!_males.Contains(secondPerson) || !_females.Contains(firstPerson)))
            {
                return false;
            }

            var p1 = _graph[firstPerson];
            if (p1 == null)
            {
                p1 = new HashSet<Person>();
                _graph.Add(firstPerson, p1);
            }

            p1.Add(secondPerson);

            var p2 = _graph[secondPerson];
            if (p2 == null)
            {
                p2 = new HashSet<Person>();
                _graph.Add(secondPerson, p2);
            }
            p2.Add(firstPerson);
            return true;
        }

        public HashSet<Person> GetAllPeople()
        {
            return new HashSet<Person>(_graph.Keys.ToList());
        }

        public HashSet<Person> GetMales()
        {
            return _males;
        }


        public HashSet<Person> GetNeighbours(Person person)
        {
            if (_graph[person] == null)
            {
                throw new NullReferenceException("Null");
            }
            return _graph[person];
        }


        public HashSet<Person> GetFemales()
        {
            return _females;
        }


        public bool IsEdge(Person firstPerson, Person secondPerson)
        {
            HashSet<Person> neighbours1 = _graph.ContainsKey(firstPerson) ? _graph[firstPerson] : null;
            if (neighbours1 == null)
            {
                return false;
            }
            HashSet<Person> neighbours2 = _graph.ContainsKey(secondPerson) ? _graph[secondPerson] : null;
            if (neighbours2 == null)
            {
                return false;
            }
            return neighbours1.Contains(secondPerson) && neighbours2.Contains(firstPerson);
        }


        public bool RemoveEdge(Person firstPerson, Person secondPerson)
        {
            if (!IsEdge(firstPerson, secondPerson))
            {
                return false;
            }
            _graph[firstPerson].Remove(secondPerson);
            _graph[secondPerson].Remove(firstPerson);
            return true;
        }

        public override String ToString()
        {
            var sb = new StringBuilder();
            foreach (var person in GetAllPeople())
            {
                sb.Append(person + ": ");
                foreach (var neighbour in GetNeighbours(person))
                {
                    sb.Append(neighbour + " ");
                }
                sb.Append("\n");
            }

            return sb.ToString();
        }
    }
}