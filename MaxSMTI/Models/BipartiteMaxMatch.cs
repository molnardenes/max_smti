﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    class BipartiteMaxMatch
    {
        private BipartiteGraph _graph;
        private Match _matching;
        private Dictionary<Person, Person> _labelling;
        private HashSet<Person> _visited;
        private Dictionary<Person, GallaiEdmondsType> _gallaiEdmonds;
        private Person _root;
        private bool _isSearching;
        private bool _isGallaiEdmonds;

        public BipartiteMaxMatch(BipartiteGraph graph)
        {
            _graph = graph;
            _matching = new Match();
            _labelling = new Dictionary<Person, Person>();
            _visited = new HashSet<Person>();
            _gallaiEdmonds = new Dictionary<Person, GallaiEdmondsType>();
            _isSearching = false;
            _isGallaiEdmonds = false;
        }

        public Match SetMaxMatch()
        {
            SetRandomMatch();
            while (FindAugmentingPath() != null)
            {
            }
            return _matching;
        }

        public Match GetMaxMatch()
        {
            return _matching;
        }

        private Person FindAugmentingPath()
        {
            foreach (var person in _graph.GetAllPeople())
            {
                if (_matching.IsFree(person))
                {
                    SetToDefault();
                    SetVisited(person);
                    _isSearching = true;
                    FindAugmentingPath(person, false);
                    if (_root != null)
                    {
                        return _root;
                    }
                }
            }
            
            return null;
        }

        private void FindAugmentingPath(Person node, bool isInMarriage)
        {
            if (_matching.IsFree(node) && !IsVisited(node))
            {
                _root = node;
                _isSearching = false;
                ImproveMatch();
                return;
            }
            SetVisited(node);
            if (_isGallaiEdmonds)
            {
                var gallaiEdmondsType = isInMarriage ? GallaiEdmondsType.Odd : GallaiEdmondsType.Even;
                _gallaiEdmonds.Add(node, gallaiEdmondsType);
            }

            foreach (var neighbour in _graph.GetNeighbours(node))
            {
                if (_matching.IsInMarriage(node, neighbour) == isInMarriage && !IsVisited(neighbour) &&
                    !IsLabelled(neighbour) && _isSearching)
                {
                    SetLabelling(neighbour, node);
                    FindAugmentingPath(neighbour, !isInMarriage);
                }
            }
        }

        private bool IsVisited(Person node)
        {
            return _visited.Contains(node);
        }

        private void SetVisited(Person node)
        {
            _visited.Add(node);
        }

        private bool IsLabelled(Person node)
        {
            return GetLabelling(node) != null;
        }

        private void SetLabelling(Person node, Person label)
        {
            _labelling.Add(node, label);
        }

        private Person GetLabelling(Person node)
        {
            return _labelling[node];
        }

        public Dictionary<Person, GallaiEdmondsType> GetGallaiEdmondsDecomposition()
        {
            if (_isGallaiEdmonds)
            {
                return _gallaiEdmonds;
            }
            _isGallaiEdmonds = true;
            FindAugmentingPath();
            return _gallaiEdmonds;
        }

        private void ImproveMatch()
        {
            if (_root == null)
            {
                throw new InvalidOperationException("A gyökérelem null.");
            }
            var remove = false;
            var currentNode = _root;
            var nextNode = GetLabelling(currentNode);
            while (nextNode != null)
            {
                if (!remove)
                {
                    _matching.AddMatchingCouple(currentNode, nextNode);
                }
                remove = !remove;
                currentNode = nextNode;
                nextNode = GetLabelling(currentNode);
            }
        }

        public Match SetRandomMatch()
        {
            SetToDefault();
            foreach (var person in _graph.GetAllPeople())
            {
                if (!_matching.IsFree(person))
                    continue;

                foreach (var neighbour in _graph.GetNeighbours(person))
                {
                    if (_matching.IsFree(neighbour))
                    {
                        _matching.AddMatchingCouple(person, neighbour);
                        break;
                    }
                }
            }
            
            return _matching;
        }

        private void SetToDefault()
        {
            _root = null;
            _labelling = new Dictionary<Person, Person>();
            _visited.Clear();
        }
    }
}
