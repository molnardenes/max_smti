﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    class MatchingPair
    {
        private readonly Male _male;
        private readonly Female _female;

        public MatchingPair(Person firstPerson, Person secondPerson)
        {
            if (firstPerson is Female && secondPerson is Male)
            {
                _female = (Female) firstPerson;
                _male = (Male) secondPerson;
            }
            else if (firstPerson is Male && secondPerson is Female)
            {
                _male = (Male) firstPerson;
                _female = (Female) secondPerson;
            }
            else
            {
                throw new ArgumentException("Csak heteroszexuális párok megengedettek.");
            }
        }

        public MatchingPair(Male male, Female female)
        {
            _male = male;
            _female = female;
        }

        public Male GetMale()
        {
            return _male;
        }

        public Female GetFemale()
        {
            return _female;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 179;

                hash = hash * 233 + _male.GetHashCode();
                hash = hash * 233 + _female.GetHashCode();

                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MatchingPair)) return false;

            var pair = (MatchingPair) obj;
            return pair.GetMale().Equals(_male) && pair.GetFemale().Equals(_female);
        }

        public override string ToString()
        {
            return "( Férfi " + _male.GetId() + ", Nő " + _female.GetId() + ")";
        }
    }
}
