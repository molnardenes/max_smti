﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxSMTI.Models
{
    class Tie
    {
        private Person _owner;
        private HashSet<Person> _tiedPersons;
        private int _choice;

        public Tie(Person person, HashSet<Person> tiedPersons, int choice)
        {
            _owner = person;
            _tiedPersons = tiedPersons;
            _choice = choice;
        }

        public HashSet<Person> GetPersons()
        {
            return _tiedPersons;
        }

        public int GetChoice()
        {
            return _choice;
        }

        public Person GetPerson()
        {           
            return GetPersons().FirstOrDefault();
        }

        public Person GetOwner()
        {
            return _owner;
        }

        public bool Remove(Person person)
        {
            return _tiedPersons.Remove(person);
        }

        public bool IsEmpty()
        {
            return _tiedPersons.Count == 0;
        }

        public bool IsContaining(Person person)
        {
            return _tiedPersons.Contains(person);
        }

        public int Size()
        {
            return _tiedPersons.Count;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(Object o)
        {
            if (o is Tie)
            {
                var tie = (Tie)o;
                return (tie.GetChoice() == GetChoice() && tie.GetOwner() == GetOwner());
            }
            return false;
        }

        public override string ToString()
        {
            return "[" + _owner + "] Döntetlen (választás = " + _choice
                    + "): " + _tiedPersons;
        }
    }
}
