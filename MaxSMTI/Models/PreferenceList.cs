﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaxSMTI.Models
{
    class PreferenceList : BoundedList<Tie>, IEnumerable<Tie>
    {
        private Dictionary<Person, Tie> _personPointer;
        private Person _owner;
        private int _capacity;

        public PreferenceList(Person owner, int capacity) : base(capacity)
        {
            this._owner = owner;
            this._capacity = capacity;
            _personPointer = new Dictionary<Person, Tie>(capacity);
        }

        public int GetCapacity()
        {
            return _capacity;
        }

        public bool Add(Person person)
        {
            if (IsFull())
            {
                throw new InvalidOperationException(
                        "A méretkorlátozást megtörné a hozzáadás.");
            }
            if (Contains(person))
            {
                return false;
            }
            var peopleSet = new HashSet<Person>();
            peopleSet.Add(person);
            AddTie(peopleSet);
            return true;
        }

        public void AddTie(HashSet<Person> people)
        {
            if (people == null)
            {
                throw new NullReferenceException("Ez a csoport tele van.");
            }
            if (Size() + people.Count > _capacity)
            {
                throw new InvalidOperationException();
            }
            AddTie(new Tie(_owner, people, Size() + 1));
        }

        public void AddTie(Person[] people)
        {
            var peopleSet = new HashSet<Person>();
            for (var i = 0; i < people.Count(); i++)
            {
                peopleSet.Add(people[i]);
            }
            AddTie(peopleSet);
        }

        public void AddTie(Tie tie)
        {
            AddElement(tie);
            foreach (var person in tie.GetPersons())
            {
                if (!_personPointer.ContainsKey(person))
                {
                    _personPointer.Add(person, tie);
                }
            }
        }


        public void BreakTie(Tie tie)
        {
            BreakTie(tie, new List<Person>(tie.GetPersons()));
        }



        public void BreakTie(Tie tie, List<Person> personsOrder)
        {
            foreach (var person in personsOrder)
            {
                PutAhead(person);
            }
        }

        private void PutAhead(Person person)
        {
            if (!Contains(person))
            {
                throw new ArgumentNullException("A(z) " + person
                        + " személy nem található.");
            }
            var tie = GetTie(person);
            var choiceNumber = tie.GetChoice();
            if (tie.Size() <= 1) return;

            var setOfPersons = new HashSet<Person>(tie.GetPersons());
            setOfPersons.Remove(person);
            var newTie = new Tie(tie.GetOwner(), setOfPersons, ++choiceNumber);
            AddElementAfter(tie, newTie);

            foreach (var activePerson in setOfPersons)
            {
                if (!_personPointer.ContainsKey(activePerson))
                {
                    tie.Remove(activePerson);
                    _personPointer.Add(activePerson, newTie);
                }
                
            }
        }


        public Tie GetFirstTie()
        {
            return IsEmpty() ? null : GetFirstNode().GetContent();
        }

        public Tie GetNextTie(Tie tie)
        {
            if (base.Contains(tie) && GetNode(tie).GetNext() != null)
            {
                return GetNode(tie).GetNext().GetContent();
            }
            return null;
        }


        public Tie GetTie(Person person)
        {
            if (person == null) return null;
            if (_personPointer.Count() == 0) return null;
            if (!_personPointer.ContainsKey(person)) return null;
            return person == null ? null : _personPointer[person];
        }
        
       
        public bool Remove(Person person)
        {
            var tie = GetTie(person);
            if (tie == null)
            {
                return false;
            }
            tie.Remove(person);
            _personPointer.Remove(person);
            if (tie.IsEmpty())
            {
                RemoveTie(tie);
            }
            return true;
        }

        private void RemoveTie(Tie tie)
        {
            RemoveNode(GetNode(tie));
        }


        public bool Contains(Person person)
        {
            return GetTie(person) != null;
        }

        public bool IsFull()
        {
            return Size() == _capacity;
        }

        public int Size()
        {
            return _personPointer.Count;
        }

        public IEnumerator<Tie> GetEnumerator()
        {
            foreach (var keyValuePair in _personPointer)
            {
                if (keyValuePair.Value == null)
                {
                    break;
                }

                yield return keyValuePair.Value;
            }
            
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
