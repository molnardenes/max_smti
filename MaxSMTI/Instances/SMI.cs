﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxSMTI.Models;

namespace MaxSMTI.Instances
{
    class Smi : SMTIOnlyWithFemaleTies
    {
        public Smi(Dictionary<int, Male> malesSmi, Dictionary<int, Female> femalesSmi) : base(malesSmi, femalesSmi)
        {
        }

        public Smi(string filename) : base(filename)
        {
        }

        protected new void AddPersons(Person person, string personsList, bool isTie, bool isMale)
        {
            if (isTie)
                throw new Exception("Az SMI-ben nem megengedettek a döntetlenek.");
            base.AddPersons(person, personsList, isTie, isMale);
        }
    }
}
