﻿using System;
using System.Collections.Generic;
using MaxSMTI.Models;

namespace MaxSMTI.Instances
{
    class SMTIOnlyWithFemaleTies : SmtiWithStrictMalesList
    {
        private bool _isOneTie;

        public SMTIOnlyWithFemaleTies(String instance) : base(instance)
        { }

        public SMTIOnlyWithFemaleTies(Dictionary<int, Male> male, Dictionary<int, Female> female) : base(male, female)
        { }


        protected override void ParseLine(Person person, string line)
        {
            _isOneTie = false;
            base.ParseLine(person, line);
        }


        protected override void AddPersons(Person person, string peopleString, bool isTie, bool isMale)
        {
            if (isTie)
            {
                _isOneTie = true;
            }
            base.AddPersons(person, peopleString, isTie, isMale);
        }
    }
}
