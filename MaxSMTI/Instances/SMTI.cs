﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using MaxSMTI.Models;
using Match = MaxSMTI.Models.Match;

namespace MaxSMTI.Instances
{
    class Smti
    {
        private string _filename;

        private int _maxId;
        private Dictionary<int, Male> _males;
        private Dictionary<int, Female> _females;
        private HashSet<Person> _everyone;

        public Smti(string filename)
        {
            _filename = filename;
            LoadInstance(filename);            
            GetAcceptablePairsOnly();
        }

        public Smti(Dictionary<int, Male> males, Dictionary<int, Female> females) : this(males, females, false)
        {
        }

        public Smti(Dictionary<int, Male> males, Dictionary<int, Female> females, bool isCopy)
        {
            if (isCopy)
            {
                _males = new Dictionary<int, Male>();

                foreach (var male in males)
                {
                    _males.Add(male.Key, male.Value);
                }

                _females = new Dictionary<int, Female>();

                foreach (var female in females)
                {
                    _females.Add(female.Key, female.Value);
                }

            }
            else
            {
                _males = males;
                _females = females;
            }
            GetAcceptablePairsOnly();
        }

        public Male GetMale(int id)
        {
            return _males[id];
        }

        public Female GetFemale(int id)
        {
            return _females[id];
        }

        public List<Male> GetMales()
        {
            var maleList = new List<Male>();
            maleList.AddRange(_males.Values);

            return maleList;
        }

        public List<Female> GetFemales()
        {
            var femaleList = new List<Female>();
            femaleList.AddRange(_females.Values);

            return femaleList;
        }

        public HashSet<Person> GetEveryone()
        {
            if (_everyone != null) return _everyone;

            _everyone = new HashSet<Person>();

            foreach (var male in GetMales())
            {
                _everyone.Add(male);
            }

            foreach (var female in GetFemales())
            {
                _everyone.Add(female);
            }

            return _everyone;
        }
                
        public HashSet<MatchingPair> GetBlockingPairs(Match match)
        {
            var blockingPairs = new HashSet<MatchingPair>();
            foreach (var male in GetMales())
            {
                foreach (var female in GetFemales())
                {
                    if (IsBlockingPair(male, female, match))
                    {
                        blockingPairs.Add(new MatchingPair(male, female));
                    }
                }
            }

            return blockingPairs;
        }

        
        public bool IsBlockingPair(Male male, Female female, Match match)
        {
            var acceptEachOther = male.GetPreferenceList().Contains(female)
                && female.GetPreferenceList().Contains(male);
            var malePrefersFemaleToCurrentPartner = match.IsFree(male)
                    || male.LikesMore(female, match.GetPartner(male));
            var femalePrefersMaleToCurrenPartner = match.IsFree(female)
                    || female.LikesMore(male, match.GetPartner(female));
            return acceptEachOther && malePrefersFemaleToCurrentPartner && femalePrefersMaleToCurrenPartner;
        }
         
        private void LoadInstance(string instance)
        {
            String line;
            int card = 0;
            int numberOfLines = 0;
            int numOfEmptyLines = 0;

            for (int i = instance.Length - 1; i >= 0; i--)
            {
                if (instance[i] == '\n')
                {
                    numOfEmptyLines++;
                }
                else
                {
                    break;
                }
            }

            instance = instance.Substring(0, instance.Length - numOfEmptyLines);

            string[] lines = instance.Split('\n');

            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i].Trim();
                if (string.IsNullOrEmpty(line))
                {
                    card = numberOfLines;
                }
                else
                {
                    CheckLine(line);
                    numberOfLines++;
                }
            }

            
            _males = new Dictionary<int, Male>(card);
            _females = new Dictionary<int, Female>(card);
            for (int i = 1; i <= card; i++)
            {
                _males.Add(i, new Male(i, card));
                _females.Add(i, new Female(i, card));
            }
            
            var isMan = true;
            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i].Trim();
                if (string.IsNullOrEmpty(line))
                {
                    isMan = false;
                }
                else
                {
                    var index = line.IndexOf(':');
                    string idString = line.Substring(0, index);
                    var id = Convert.ToInt32(idString);
                    Person person;
                    if (isMan)
                    {
                        person = _males[id];
                    }
                    else
                    {
                        person = _females[id];
                    }
                    ParseLine(person, line.Substring(idString.Length + 1).Trim());
                }
            }
        }


        private void GetAcceptablePairsOnly()
        {
            foreach (var male in GetMales())
            {
                foreach (var female in GetFemales())
                {
                    if (!male.IsAcceptable(female))
                    {
                        female.RemoveFromPreferenceList(male);
                    }
                    if (!female.IsAcceptable(male))
                    {
                        male.RemoveFromPreferenceList(female);
                    }
                }
            }
        }

        public int GetCardinality()
        {
            return Math.Max(_males.Count(), _females.Count());
        }

        public int GetCapacity()
        {
            return _males.Count() + _females.Count();
        }

        public void BreakMaleTies()
        {
            foreach (var male in GetMales())
            {
                var preferenceList = male.GetPreferenceList();
                foreach (var tie in preferenceList)
                {
                    preferenceList.BreakTie(tie);
                }
            }
        }

        public void BreakFemaleTies()
        {
            foreach (var female in GetFemales())
            {
                var preferenceList = female.GetPreferenceList();
                foreach (var tie in preferenceList)
                {
                    preferenceList.BreakTie(tie);
                }
            }
        }

        public void BreakTies()
        {
            BreakMaleTies();
            BreakFemaleTies();
        }

        public Smti GetCopyOfInstance()
        {
            var males = new Dictionary<int, Male>();
            var females = new Dictionary<int, Female>();

            foreach (var male in GetMales())
            {
                males.Add(male.GetId(), new Male(male.GetId(), male.GetPreferenceList().GetCapacity()));
            }

            foreach (var female in GetFemales())
            {
                females.Add(female.GetId(), new Female(female.GetId(), female.GetPreferenceList().GetCapacity()));
            }

            foreach (var male in GetMales())
            {
                var preferenceList = male.GetPreferenceList();

                foreach (var tie in preferenceList)
                {
                    var femalesNodes = new HashSet<Person>();
                    foreach (var person in tie.GetPersons())
                    {
                        femalesNodes.Add(females[person.GetId()]);
                    }
                    males[male.GetId()].GetPreferenceList().AddTie(femalesNodes);
                }
            }

            foreach (var female in GetFemales())
            {
                var preferenceList = female.GetPreferenceList();

                foreach (var tie in preferenceList)
                {
                    var malesNodes = new HashSet<Person>();
                    foreach (var person in tie.GetPersons())
                    {
                        malesNodes.Add(males[person.GetId()]);
                    }
                    females[female.GetId()].GetPreferenceList().AddTie(malesNodes);
                }
            }

            return new Smti(males, females);
        }
                
        public Smi GetSmi()
        {
            var smti = GetCopyOfInstance();
            var malesSmi = new Dictionary<int, Male>(smti.GetMales().Count());
            var femalesSmi = new Dictionary<int, Female>(smti.GetFemales().Count());

            foreach (var male in smti.GetMales())
            {
                var preferenceList = male.GetPreferenceList();
                foreach (var tie in preferenceList)
                {
                    preferenceList.BreakTie(tie);
                }
                malesSmi.Add(male.GetId(), male);
            }

            foreach (var female in smti.GetFemales())
            {
                var preferenceList = female.GetPreferenceList();
                foreach (var tie in preferenceList)
                {
                    preferenceList.BreakTie(tie);
                }
                femalesSmi.Add(female.GetId(), female);
            }

            return new Smi(malesSmi, femalesSmi);
        }
                
        public SmtiWithStrictMalesList GetSmtiWithStrictMalesList()
        {
            var instanceCopy = GetCopyOfInstance();
            instanceCopy.BreakMaleTies();
            return new SmtiWithStrictMalesList(instanceCopy._males, instanceCopy._females);
        }
                
        public SmtiWithStrictFemalesList GetSmtiWithStrictFemalesList(Dictionary<Person, double> extraScores)
        {
            var instanceCopy = GetCopyOfInstance();

            foreach (var keyValuePair in extraScores)
            {
                var extraScore = keyValuePair.Value;
                var person = keyValuePair.Key;
                if (extraScore != 0.5 || person.GetSex() != Sex.Male) continue;
                foreach (var female in GetFemales())
                {
                    female.PromotePersonAhead(person);
                }
            }

            instanceCopy.BreakFemaleTies();
            return new SmtiWithStrictFemalesList(instanceCopy._males, instanceCopy._females);
        }

        public SMTIOnlyWithFemaleTies GetSmtiOnlyWithFemaleTies()
        {
            var copy = GetCopyOfInstance();
            copy.BreakMaleTies();

            foreach (var female in copy.GetFemales())
            {
                foreach (var tie in female.GetPreferenceList())
                {
                    female.GetPreferenceList().BreakTie(tie);
                }
            }

            return new SMTIOnlyWithFemaleTies(copy._males, copy._females);
        }
                
        protected void CheckLine(String line)
        {
            char c;
            bool isBracket = false;
            var index = 0;

            c = line[index++];

            while (c != ':')
            {
                c = line[index++];
            }
            for (; index < line.Length; index++)
            {
                c = line[index];
                if (c == '(')
                {
                    if (isBracket)
                    {
                        throw new Exception("Hibás zárójelezés.");
                    }
                    isBracket = true;
                    continue;
                }
                if (c == ')')
                {
                    if (!isBracket)
                    {
                        throw new Exception("Hibás zárójelezés.");
                    }
                    isBracket = false;
                    continue;
                }
                if (c != ' ' && !Char.IsNumber(c))
                {
                    throw new Exception("Csak szám lehet.");
                }
            }
            if (isBracket)
            {
                throw new Exception("Hibás zárójelezés.");
            }
        }
                
        protected virtual void ParseLine(Person person, String line)
        {
            char c;
            var index = 0;
            var isTie = false;
            var isMan = person is Male;
            var sb = new StringBuilder();
            while (index < line.Length)
            {
                c = line[index];
                if (c == '(' || c == ')')
                {
                    AddPersons(person, sb.ToString(), isTie, isMan);
                    sb = new StringBuilder();
                    isTie = !isTie;
                }
                else
                {
                    sb.Append(c);
                }
                index++;
            }

            if (sb.Length > 0)
            {
                AddPersons(person, sb.ToString(), isTie, isMan);
            }
        }
        protected virtual void AddPersons(Person person, string personList, bool isTie, bool isMale)
        {
            personList = personList.Trim();
            if (personList.Length > 0)
            {
                var ids = personList.Split(' ');
                var persons = new Person[ids.Length];
                if (isMale)
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        if (ids[i].Length == 0)
                        {
                            continue;
                        }
                        persons[i] = _females[Convert.ToInt32(ids[i])];
                    }
                }
                else
                {
                    for (int i = 0; i < ids.Length; i++)
                    {
                        if (ids[i].Length == 0)
                        {
                            continue;
                        }
                        persons[i] = _males[Convert.ToInt32(ids[i])];
                    }
                }
                if (isTie)
                {
                    person.GetPreferenceList().AddTie(persons);
                }
                else
                {
                    for (int i = 0; i < persons.Length; i++)
                    {
                        person.GetPreferenceList().Add(persons[i]);
                    }
                }
            }
        }

        public string GetInstance()
        {
            var sb = new StringBuilder();
            for (int i = 1; i <= _males.Count(); i++)
            {
                var maleReplace = new Regex("Férfi\\(");
                var instanceText = maleReplace.Replace(_males[i].ToString(), "", 1);
                maleReplace = new Regex("\\)");
                instanceText = maleReplace.Replace(instanceText, "", 1);
                sb.Append(instanceText + "\n");
            }
            sb.Append("\n");

            for (int i = 1; i <= _females.Count(); i++)
            {
                var femaleReplace = new Regex("Nő\\(");
                var instanceText = femaleReplace.Replace(_females[i].ToString(), "", 1);
                femaleReplace = new Regex("\\)");
                instanceText = femaleReplace.Replace(instanceText, "", 1);
                sb.Append(instanceText + "\n");
            }
            return sb.ToString();
        }


        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 1; i <= _males.Count(); i++)
            {
                sb.Append(_males[i] + "\n");
            }
            for (int i = 1; i <= _females.Count(); i++)
            {
                sb.Append(_females[i] + "\n");
            }
            return sb.ToString();
        }


    }
}
