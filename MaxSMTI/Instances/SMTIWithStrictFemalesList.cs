﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxSMTI.Models;

namespace MaxSMTI.Instances
{
    class SmtiWithStrictFemalesList : Smti
    {
        public SmtiWithStrictFemalesList(Dictionary<int, Male> males, Dictionary<int, Female> females) : base(males, females)
        {
        }

        public SmtiWithStrictFemalesList(string filename) : base(filename)
        {
        }

        protected new void AddPersons(Person person, string personsList, bool isTie, bool isMale)
        {
            if (!isMale && isTie)
                throw new Exception("Nő és döntetlen nem adható hozzá.");
            base.AddPersons(person, personsList, isTie, isMale);
        }
    }
}
