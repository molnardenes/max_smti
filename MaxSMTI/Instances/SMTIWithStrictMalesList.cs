﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxSMTI.Models;

namespace MaxSMTI.Instances
{
    class SmtiWithStrictMalesList : Smti
    {
        
        public SmtiWithStrictMalesList(Dictionary<int, Male> males, Dictionary<int, Female> females) : base(males, females)
        {
        }

        public SmtiWithStrictMalesList(string filename) : base(filename)
        {
        }
        
        protected override void AddPersons(Person person, string personsList, bool isTie, bool isMale)
        {
            if (isMale && isTie)
                throw new Exception("Döntetlen és férfi nem adható át.");

            base.AddPersons(person, personsList, isTie, isMale);
        }
    }
}
