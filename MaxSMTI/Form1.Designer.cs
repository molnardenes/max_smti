﻿namespace MaxSMTI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateInstanceButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tieLabel = new System.Windows.Forms.Label();
            this.prefSizeLabel = new System.Windows.Forms.Label();
            this.cardinalLabel = new System.Windows.Forms.Label();
            this.runningTimeLabel = new System.Windows.Forms.Label();
            this.maxSmtiLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rtTable = new System.Windows.Forms.Label();
            this.mLabel = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // generateInstanceButton
            // 
            this.generateInstanceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateInstanceButton.Location = new System.Drawing.Point(284, 13);
            this.generateInstanceButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.generateInstanceButton.Name = "generateInstanceButton";
            this.generateInstanceButton.Size = new System.Drawing.Size(87, 30);
            this.generateInstanceButton.TabIndex = 0;
            this.generateInstanceButton.Text = "Futtatás";
            this.generateInstanceButton.UseVisualStyleBackColor = true;
            this.generateInstanceButton.Click += new System.EventHandler(this.generateInstanceButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MaxSMTI.Properties.Resources.Time_Date_icon;
            this.pictureBox1.Location = new System.Drawing.Point(270, 116);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.tieLabel);
            this.panel1.Controls.Add(this.prefSizeLabel);
            this.panel1.Controls.Add(this.cardinalLabel);
            this.panel1.Controls.Add(this.runningTimeLabel);
            this.panel1.Controls.Add(this.maxSmtiLabel);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.rtTable);
            this.panel1.Controls.Add(this.mLabel);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(12, 51);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(359, 197);
            this.panel1.TabIndex = 2;
            // 
            // tieLabel
            // 
            this.tieLabel.AutoSize = true;
            this.tieLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.tieLabel.Location = new System.Drawing.Point(130, 168);
            this.tieLabel.Name = "tieLabel";
            this.tieLabel.Size = new System.Drawing.Size(13, 17);
            this.tieLabel.TabIndex = 14;
            this.tieLabel.Text = "-";
            // 
            // prefSizeLabel
            // 
            this.prefSizeLabel.AutoSize = true;
            this.prefSizeLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.prefSizeLabel.Location = new System.Drawing.Point(196, 151);
            this.prefSizeLabel.Name = "prefSizeLabel";
            this.prefSizeLabel.Size = new System.Drawing.Size(13, 17);
            this.prefSizeLabel.TabIndex = 13;
            this.prefSizeLabel.Text = "-";
            // 
            // cardinalLabel
            // 
            this.cardinalLabel.AutoSize = true;
            this.cardinalLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.cardinalLabel.Location = new System.Drawing.Point(86, 134);
            this.cardinalLabel.Name = "cardinalLabel";
            this.cardinalLabel.Size = new System.Drawing.Size(13, 17);
            this.cardinalLabel.TabIndex = 12;
            this.cardinalLabel.Text = "-";
            // 
            // runningTimeLabel
            // 
            this.runningTimeLabel.AutoSize = true;
            this.runningTimeLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.runningTimeLabel.Location = new System.Drawing.Point(69, 60);
            this.runningTimeLabel.Name = "runningTimeLabel";
            this.runningTimeLabel.Size = new System.Drawing.Size(13, 17);
            this.runningTimeLabel.TabIndex = 11;
            this.runningTimeLabel.Text = "-";
            // 
            // maxSmtiLabel
            // 
            this.maxSmtiLabel.AutoSize = true;
            this.maxSmtiLabel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.maxSmtiLabel.Location = new System.Drawing.Point(191, 43);
            this.maxSmtiLabel.Name = "maxSmtiLabel";
            this.maxSmtiLabel.Size = new System.Drawing.Size(13, 17);
            this.maxSmtiLabel.TabIndex = 10;
            this.maxSmtiLabel.Text = "-";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Döntetlenek száma:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 151);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Átlagos preferencialista-méret:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(1, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(269, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "Algoritmus futási eredménye";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(1, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Példány adatai";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Számosság:";
            // 
            // rtTable
            // 
            this.rtTable.AutoSize = true;
            this.rtTable.Location = new System.Drawing.Point(3, 60);
            this.rtTable.Name = "rtTable";
            this.rtTable.Size = new System.Drawing.Size(60, 17);
            this.rtTable.TabIndex = 4;
            this.rtTable.Text = "Futásidő:";
            // 
            // mLabel
            // 
            this.mLabel.AutoSize = true;
            this.mLabel.Location = new System.Drawing.Point(3, 43);
            this.mLabel.Name = "mLabel";
            this.mLabel.Size = new System.Drawing.Size(182, 17);
            this.mLabel.TabIndex = 3;
            this.mLabel.Text = "Maximális párosítások száma:";
            // 
            // comboBox1
            // 
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Halldórson",
            "Irving-Manlove",
            "Iwama-Miyazaki-Yamauchi"});
            this.comboBox1.Location = new System.Drawing.Point(12, 13);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(264, 25);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 258);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.generateInstanceButton);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "Szakdolgozat";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button generateInstanceButton;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label mLabel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label rtTable;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label tieLabel;
        private System.Windows.Forms.Label prefSizeLabel;
        private System.Windows.Forms.Label cardinalLabel;
        private System.Windows.Forms.Label runningTimeLabel;
        private System.Windows.Forms.Label maxSmtiLabel;
    }
}

