﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxSMTI.Models;
using MaxSMTI.Instances;

namespace MaxSMTI.Algorithms
{
    class GaleShapley
    {
        protected Match StableMarriage;

        protected List<Male> Males;
        protected List<Female> Females;
        protected Queue<Male> FreeMales;
        protected Queue<Female> FreeFemales;

        public GaleShapley(SmtiWithStrictMalesList smti) : this(smti.GetMales(), smti.GetFemales())
        { }

        public GaleShapley(SmtiWithStrictFemalesList smti) : this(smti.GetMales(), smti.GetFemales())
        { }

        private GaleShapley(List<Male> males, List<Female> females)
        {
            Males = males;
            Females = females;
            StableMarriage = new Match();
        }

        public Match SetMaleProposedStableMarriage()
        {
            SetFreeMales();
            while (FreeMales.Any())
            {
                MakeProposal(FreeMales.Dequeue());
            }
            return StableMarriage;
        }

        protected void SetFreeMales()
        {
            FreeMales = new Queue<Male>(Males);
        }

        public Match SetFemaleProposedStableMarriage()
        {
            SetFreeFemales();
            while (FreeFemales.Any())
            {
                MakeProposal(FreeFemales.Dequeue());
            }
            return StableMarriage;
        }

        protected void SetFreeFemales()
        {
            FreeFemales = new Queue<Female>(Females);
        }

        public Match SetStableMarriage()
        {
            return SetMaleProposedStableMarriage();
        }

        protected void MakeProposal(Person person)
        {
            if (person == null || person.HasAlreadyProposedToAllPersons())
            {
                return;
            }

            var nextPerson = person.GetNextPerson();
            var currentPartner = StableMarriage.GetPartner(nextPerson);


            if (!Males.Contains(nextPerson) && !Females.Contains(nextPerson))
            {
                MakeProposal(person);
            }

            else if (IsPartnerBetter(nextPerson, person, currentPartner))
            {
                if (person != null && nextPerson != null)
                {
                    StableMarriage.AddMatchingCouple(person, nextPerson);
                    MakeProposal(currentPartner);
                }

            }

            else
            {
                MakeProposal(person);
            }
        }

        protected bool IsPartnerBetter(Person person, Person newPartner, Person oldPartner)
        {
            if (person == null)
            {
                throw new NullReferenceException(
                        "Az érték nem lehet null.");
            }

            return person.LikesMore(newPartner, oldPartner);
        }

        public Match GetStableMarriage()
        {
            return StableMarriage;
        }


    }
}
