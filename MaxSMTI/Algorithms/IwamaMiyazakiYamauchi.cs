﻿using System.Collections.Generic;
using System.Linq;
using MaxSMTI.Instances;
using MaxSMTI.Models;

namespace MaxSMTI.Algorithms
{
    class IwamaMiyazakiYamauchi
    {
        private Smti _smti;
        private Match _stableMatch;

        public IwamaMiyazakiYamauchi(Smti smti)
        {
            _smti = smti;
        }

        public Match SetStableMarriage()
        {
            _stableMatch = new GaleShapley(_smti.GetSmi()).SetStableMarriage();

            while (true)
            {
                var matchMComma = IncreaseFromMaleSide(_stableMatch, true);
                if (matchMComma != null)
                {
                    _stableMatch = Stabilize(matchMComma);
                    continue;
                }
                matchMComma = IncreaseFromMaleSide(_stableMatch, false);
                if (matchMComma != null)
                {
                    _stableMatch = Stabilize(matchMComma);
                    continue;
                }
                else
                {
                    break;
                }
            }
            return _stableMatch;
        }


        protected Match IncreaseFromMaleSide(Match stableMatch, bool fromMaleSide)
        {
            var matchM = new Match(stableMatch);
            var couplesFromMatchM = matchM.GetMatchingCouples();

            var setOfMalesV1 = new HashSet<Person>();
            if (fromMaleSide)
            {
                setOfMalesV1.UnionWith(_smti.GetMales());
            }
            else
            {
                setOfMalesV1.UnionWith(_smti.GetFemales());
            }

            var setOfFemalesU1 = new HashSet<Person>();
            foreach (var matchingPair in couplesFromMatchM)
            {
                var male = _smti.GetMale(matchingPair.GetMale().GetId());
                var female = _smti.GetFemale(matchingPair.GetFemale().GetId());

                if (fromMaleSide)
                {
                    setOfMalesV1.Remove(male);
                    setOfFemalesU1.Add(female);
                }
                else
                {
                    setOfMalesV1.Remove(female);
                    setOfFemalesU1.Add(male);
                }
            }



            var graphG1 = new BipartiteGraph(setOfMalesV1, setOfFemalesU1);

            foreach (var female in setOfFemalesU1)
            {
                var tie = female.GetPreferenceList().GetTie(stableMatch.GetPartner(female));

                foreach (var male in setOfMalesV1)
                {
                    if (tie.GetPersons().Contains(male))
                    {
                        graphG1.AddEdge(male, female);
                    }
                }
            }

            var maxMatch = new BipartiteMaxMatch(graphG1);

            var maxMatchP = maxMatch.SetMaxMatch();

            var matchMComma = new Match();

            foreach (var matchingPair in couplesFromMatchM)
            {
                Person person;
                if (fromMaleSide)
                {
                    person = matchingPair.GetFemale();
                }
                else
                {
                    person = matchingPair.GetMale();
                }

                if (!maxMatchP.IsFree(person))
                {
                    var male = _smti.GetMale(matchingPair.GetMale().GetId());
                    var female = _smti.GetFemale(matchingPair.GetFemale().GetId());
                    matchMComma.AddMatchingCouple(male, female);
                }
            }

            var matchM1 = new Match();
            foreach (var matchingPair in couplesFromMatchM)
            {
                if (!matchMComma.Contains(matchingPair))
                {
                    matchM1.AddMatchingCouple(matchingPair);
                }
            }

            matchM1.AddMatchingCouples(maxMatchP.GetMatchingCouples());

            var setOfMalesU2 = new HashSet<Person>();
            var setOfFemalesV2 = new HashSet<Person>();
            if (fromMaleSide)
            {
                setOfMalesU2.UnionWith(matchMComma.GetMales());
                setOfFemalesV2.UnionWith(matchM1.GetFemales());
            }
            else
            {
                setOfMalesU2.UnionWith(matchMComma.GetFemales());
                setOfFemalesV2.UnionWith(matchM1.GetMales());
            }

            var graphG2 = new BipartiteGraph(setOfMalesU2,
                    setOfFemalesV2);

            HashSet<Person> setOfFemalesF;

            if (fromMaleSide)
            {
                setOfFemalesF = new HashSet<Person>(_smti.GetFemales());
            }
            else
            {
                setOfFemalesF = new HashSet<Person>(_smti.GetMales());
            }

            setOfFemalesF.ExceptWith(setOfFemalesV2);


            foreach (var male in setOfMalesU2)
            {
                var matchMm = new Match(matchM1);
                var bestSuitablePartner = GetBestSuitablePartner(male, setOfFemalesF);
                if (bestSuitablePartner != null)
                {
                    matchMm.AddMatchingCouple(new MatchingPair(male, bestSuitablePartner));
                    foreach (var blockingPair in _smti.GetBlockingPairs(matchM1))
                    {
                        Person person;
                        if (fromMaleSide)
                        {
                            person = blockingPair.GetFemale();
                        }
                        else
                        {
                            person = blockingPair.GetMale();
                        }
                        if (setOfFemalesV2.Contains(person))
                        {
                            graphG2.AddEdge(male, person);
                        }
                    }
                }
            }



            var smtiCopy = _smti.GetCopyOfInstance();
            foreach (var male in smtiCopy.GetMales())
            {
                foreach (var female in smtiCopy.GetFemales())
                {
                    if (!graphG2.IsEdge(male, female))
                    {
                        male.RemoveFromPreferenceList(female);
                    }
                }
            }

            var smi = smtiCopy.GetSmi();

            var galeShapley = new GaleShapley(smi);

            Match matchQ;
            if (fromMaleSide)
            {
                matchQ = galeShapley.SetMaleProposedStableMarriage();
            }
            else
            {
                matchQ = galeShapley.SetFemaleProposedStableMarriage();
            }

            HashSet<Person> femalesInQ;
            if (fromMaleSide)
            {
                femalesInQ = new HashSet<Person>(matchQ.GetFemales());
            }
            else
            {
                femalesInQ = new HashSet<Person>(matchQ.GetMales());
            }


            var matchMCommaComma = new Match();
            foreach (var matchingCouple in matchM1.GetMatchingCouples())
            {
                Person person;
                if (fromMaleSide)
                {
                    person = matchingCouple.GetFemale();
                }
                else
                {
                    person = matchingCouple.GetMale();
                }
                if (femalesInQ.Contains(person))
                {
                    matchMCommaComma.AddMatchingCouple(matchingCouple);
                }
            }


            var matchM2 = new Match();
            foreach (var matchingCouple in matchM1.GetMatchingCouples())
            {
                if (!matchMCommaComma.Contains(matchingCouple))
                {
                    matchM2.AddMatchingCouple(matchingCouple);
                }
            }

            matchM2.AddMatchingCouples(matchQ.GetMatchingCouples());


            HashSet<Person> malesInDiff;
            if (fromMaleSide)
            {
                malesInDiff = new HashSet<Person>(matchMComma.GetMales());
                malesInDiff.ExceptWith(matchM2.GetMales());
            }
            else
            {
                malesInDiff = new HashSet<Person>(matchMComma.GetFemales());
                malesInDiff.ExceptWith(matchM2.GetFemales());
            }

            foreach (var male in malesInDiff)
            {
                var bestSuitablePartner = GetBestSuitablePartner(male, setOfFemalesF);
                if (bestSuitablePartner != null)
                {
                    var female = bestSuitablePartner;
                    var matchMa = new Match(matchM2);
                    matchMa.AddMatchingCouple(new MatchingPair(male, female));

                    if (IsSatisfyingProperty(matchMa))
                    {
                        return matchMa;
                    }
                }
            }
            return null;
        }

        private bool IsSatisfyingProperty(Match match)
        {
            foreach (var blockingPair in _smti.GetBlockingPairs(match))
            {

                if (!(match.IsFree(blockingPair.GetMale()) ^ match.IsFree(blockingPair.GetFemale())))
                {
                    return false;
                }
            }

            return true;

        }


        protected Match Stabilize(Match match)
        {

            HashSet<MatchingPair> blockingPairWithSingleMaleAndMatchedFemale = GetBlockingPairOfSingleMaleAndMatchedFemale(match);

            for (var i = blockingPairWithSingleMaleAndMatchedFemale.Count; i >= 0; i--)
            {
                var couple = blockingPairWithSingleMaleAndMatchedFemale.ToList()[i];
                var male = couple.GetMale();
                var female = (Female)FindBestPartner(male, blockingPairWithSingleMaleAndMatchedFemale);
                match.RemoveMatchingCouple(new MatchingPair((Male)match.GetPartner(female), female));
                match.AddMatchingCouple(new MatchingPair(male, female));
                blockingPairWithSingleMaleAndMatchedFemale = GetBlockingPairOfSingleMaleAndMatchedFemale(match);
            }

            var blockingPairWithSingleFemale = GetBlockingPairWhereFemaleIsSingle(match);

            for (var i = blockingPairWithSingleFemale.Count; i >= 0; i--)
            {
                var couple = blockingPairWithSingleFemale.ToList()[i];
                var female = couple.GetFemale();

                var male = (Male)FindBestPartner(female, blockingPairWithSingleFemale);

                if (!match.IsFree(male))
                {
                    match.RemoveMatchingCouple(new MatchingPair(male, (Female)match.GetPartner(male)));
                    match.AddMatchingCouple(new MatchingPair(male, female));
                }
                else
                {
                    match.AddMatchingCouple(new MatchingPair(male, female));
                }
                blockingPairWithSingleFemale = GetBlockingPairWhereFemaleIsSingle(match);
            }

            return match;
        }

        private Person FindBestPartner(Person person, HashSet<MatchingPair> couples)
        {
            foreach (var tie in person.GetPreferenceList())
            {
                foreach (var partner in tie.GetPersons())
                {
                    if (couples.Contains(new MatchingPair(person, partner)))
                    {
                        return partner;
                    }
                }
            }

            return null;
        }

        private Person GetBestSuitablePartner(Person person, HashSet<Person> setOfPossiblePersons)
        {
            Person bestPartner = null;

            foreach (var possiblePartner in setOfPossiblePersons)
            {
                if (person.LikesMore(possiblePartner, bestPartner))
                {
                    bestPartner = possiblePartner;
                }
            }

            return bestPartner;
        }

        private HashSet<MatchingPair> GetBlockingPairOfSingleMaleAndMatchedFemale(Match match)
        {
            var blockingPairWithSingleMaleAndMatchedFemale = new HashSet<MatchingPair>();
            foreach (var blockingPair in _smti.GetBlockingPairs(match))
            {
                if (match.IsFree(blockingPair.GetMale()) && !match.IsFree(blockingPair.GetFemale()))
                {
                    blockingPairWithSingleMaleAndMatchedFemale.Add(blockingPair);
                }
            }
            return blockingPairWithSingleMaleAndMatchedFemale;
        }


        private HashSet<MatchingPair> GetBlockingPairWhereFemaleIsSingle(Match match)
        {
            var blockingPairWithSingleFemale = new HashSet<MatchingPair>();

            foreach (var blockingPair in _smti.GetBlockingPairs(match))
            {
                if (match.IsFree(blockingPair.GetFemale()))
                {
                    blockingPairWithSingleFemale.Add(blockingPair);
                }
            }

            return blockingPairWithSingleFemale;

        }


    }
}
