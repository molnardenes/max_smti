﻿using System;
using System.Collections.Generic;
using System.Linq;
using MaxSMTI.Instances;
using MaxSMTI.Models;
using MaxSMTI.Algorithms;


namespace MaxSMTI.Algorithms
{
    class IrvingManloveApproximation
    {
        private SMTIOnlyWithFemaleTies _smtiOnlyWithFemaleTies;
        private SMTIOnlyWithFemaleTies _auxSmtiOnlyWithFemaleTies;
        private HashSet<Male> _males;
        private Match _stableMatching;

        public IrvingManloveApproximation(SMTIOnlyWithFemaleTies smtiOnlyWithFemaleTies)
        {
            _smtiOnlyWithFemaleTies = smtiOnlyWithFemaleTies;
            _auxSmtiOnlyWithFemaleTies = smtiOnlyWithFemaleTies.GetSmtiOnlyWithFemaleTies();
            _males = new HashSet<Male>();
            _stableMatching = new Match();
        }

        public Match SetStableMarriage()
        {
            return SetMaleProposedStableMarriage();
        }

        private Match SetMaleProposedStableMarriage()
        {
            Round1();
            Round2();
            Round3();
            return new GaleShapley(_auxSmtiOnlyWithFemaleTies).SetStableMarriage();
            
        }


        private void Round1()
        {
            var female = GetFemaleForProposal();
            while (female != null)
            {
                var male = female.GetFirstMale();
                _stableMatching.AddMatchingCouple(male, female);
                male.RemoveWorseFromPreferenceList(female);

                _males.Add(male);
                female = GetFemaleForProposal();
            }
        }

        private void Round2()
        {
            var freeMales = new HashSet<Person>();
            var freeFemales = new HashSet<Person>();

            foreach (var female in _auxSmtiOnlyWithFemaleTies.GetFemales())
            {
                if (_stableMatching.IsFree(female))
                    freeFemales.Add(female);
            }

            foreach (var male in _auxSmtiOnlyWithFemaleTies.GetMales())
            {
                if (_stableMatching.IsFree(male))
                    freeMales.Add(male);
            }

            var graph = new BipartiteGraph(freeMales, freeFemales);

            foreach (var freeMale in freeMales)
            {
                foreach (var freeFemale in freeFemales)
                {
                    if (freeMale.IsAcceptable(freeFemale))
                        graph.AddEdge(freeMale, freeFemale);
                }
            }

            var maxMatch = new BipartiteMaxMatch(graph).SetMaxMatch();
            foreach (var male in maxMatch.GetMales())
            {
                var female = maxMatch.GetPartner(male);
                female.PromotePersonAhead(male);
            }

            Round1();
        }

        private void Round3()
        {
            var males = new HashSet<Male>();

            foreach (var male in _auxSmtiOnlyWithFemaleTies.GetMales())
            {
                if (!_males.Contains(male))
                    males.Add(male);
            }

            foreach (var female in _auxSmtiOnlyWithFemaleTies.GetFemales())
            {
                foreach (var male in males)
                {
                    female.PromotePersonAhead(male);
                }
            }
            
            _auxSmtiOnlyWithFemaleTies.BreakTies();
        }

        private Female GetFemaleForProposal()
        {
            foreach (var female in _smtiOnlyWithFemaleTies.GetFemales())
            {
                if (_stableMatching.IsFree(female) && !female.GetPreferenceList().IsEmpty() && !female.IsPersonInTie(female.GetFirstMale()))
                    return female;
            }
            
            return null;
        }

    }
}
