﻿using System;
using System.Collections.Generic;
using MaxSMTI.Instances;
using MaxSMTI.Models;

namespace MaxSMTI.Algorithms
{
    class Halldorson
    {
        private Smti _smti;
        private Random _random;

        public Halldorson(Smti smti)
        {
            _smti = smti;
            _random = new Random(smti.GetHashCode());
        }
        
        public Match SetStableMarriage()
        {
            foreach (var male in _smti.GetMales())
            {
                foreach (var tie in male.GetPreferenceList())
                {
                    if (tie.Size() > 1)
                    {
                        var orderList = GetRandomOrder(tie.GetPersons());
                        male.GetPreferenceList().BreakTie(tie, orderList);
                    }
                    male.GetPreferenceList().GetNextTie(tie);
                }
            }

            foreach (var female in _smti.GetFemales())
            {
                foreach (var tie in female.GetPreferenceList())
                {
                    if (tie.Size() > 1)
                    {
                        var orderList = GetRandomOrder(tie.GetPersons());
                        female.GetPreferenceList().BreakTie(tie, orderList);
                    }
                    female.GetPreferenceList().GetNextTie(tie);
                }
            }

            return new GaleShapley(_smti.GetSmi()).SetStableMarriage();
        }

 
        private List<Person> GetRandomOrder(HashSet<Person> persons)
        {
            var list = new List<Person>(persons);
            var randomList = new List<Person>();

            for (var i = list.Count - 1; i >= 0; i--)
            {
                var index = _random.Next(list.Count);
                randomList.Add(list[index]);
                list.Remove(list[index]);
            }

            return randomList;
        }
    }
}
