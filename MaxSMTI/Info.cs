﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaxSMTI.Instances;
using MaxSMTI.Models;

namespace MaxSMTI
{
    class Info
    {
        private Smti _smti;
        private int _tieLengthMax;
        private int _tieCount;
        private int _tieLength;
        private int _preferenceListLength;
        private int _preferenceListTieCount;
        private bool _hasEveryonePreferenceList;

        public Info(Smti smti)
        {
            _smti = smti;
            _hasEveryonePreferenceList = true;
            foreach (var person in _smti.GetEveryone())
            {
                var preferenceList = person.GetPreferenceList();
                var hasTie = false;
                var hasPreferenceList = false;

                foreach (var tie in preferenceList)
                {
                    hasPreferenceList = true;
                    _preferenceListLength += tie.Size();

                    if (tie.Size() > 1)
                    {
                        _tieLength += tie.Size();
                        _tieCount++;
                        hasTie = true;
                    }

                    if (tie.Size() > _tieLengthMax)
                        _tieLengthMax = tie.Size();
                }

                if (hasTie) _preferenceListTieCount++;
                if (!hasPreferenceList) _hasEveryonePreferenceList = false;
            }
        }

        public int GetCardinality()
        {
            return _smti.GetCardinality();
        }

        public int GetPreferenceListLengthAverage()
        {
            return Convert.ToInt32(Math.Round((double) _preferenceListLength/(double) (2 * GetCardinality())));
            
        }

        public int GetTieLengthMax()
        {
            return _tieLengthMax;
        }

        public int GetTieLengthAverage()
        {
            return Convert.ToInt32(Math.Round((double)_tieLength / (double) _tieCount));
            
        }

        public int GetPreferenceListTieCount()
        {
            return _preferenceListTieCount;
        }

        public bool HasEveryonePreferenceList()
        {
            return _hasEveryonePreferenceList;
        }

        public int GetTieCount()
        {
            return _tieCount;
        }      

    }
}
